# Release Note

## 2023/12/7

Commit: cbad8ce916ff4e676defa13563b660c6673d28e0

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev

- Branch: NP6_0

- Commit:   c3532ffd7cf5fed19b696d2f5a354f6f13fb5d59

  |               Binary               |                  Commit                  |         Repo/Branch         | Module |
  | :--------------------------------: | :--------------------------------------: | :-------------------------: | :----: |
  |        libapu_mdw.so.2.0.0         | 88f5f1215381087737b262747ff2901260c0fef4 |    aiv-mp-hardknott.mp1     |        |
  |     libapu_mdw_batch.so.2.0.0      | 88f5f1215381087737b262747ff2901260c0fef4 |    aiv-mp-hardknott.mp1     |        |
  |      libapusys_edma.so.3.0.0       | 88f5f1215381087737b262747ff2901260c0fef4 |    aiv-mp-hardknott.mp1     |        |
  |        libmdla_ut.so.2.0.0         | c3532ffd7cf5fed19b696d2f5a354f6f13fb5d59 |       alps-mp-t0.mp1        |        |
  |        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |       alps-mp-t0.mp1        |        |
  |          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |       alps-mp-t0.mp5        |        |
  | libneuronusdk_runtime.mtk.so.6.3.3 | dff37081fa477fd861f859e782c4b12bcd71ab66 |           NP6_0             |        | 
  | libneuronusdk_adapter.mtk.so.6.3.3 | dff37081fa477fd861f859e782c4b12bcd71ab66 |           NP6_0             |        | 
  |     libneuron_platform.vpu.so      | dff37081fa477fd861f859e782c4b12bcd71ab66 |           NP6_0             |        | 
  |             ncc-tflite             | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |
  |              neuronrt              | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |
  |         runtime_api_sample         | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |


Changes:
- Turn off libcmdl warning log if GPU is disabled
- Update midware library

## 2023/12/5


Commit: ae952e0a666eb83a53af68b476c873abfaba14fe

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev

- Branch: NP6_0

- Commit:   dff37081fa477fd861f859e782c4b12bcd71ab66

  |               Binary               |                  Commit                  |         Repo/Branch         | Module |
  | :--------------------------------: | :--------------------------------------: | :-------------------------: | :----: |
  |        libapu_mdw.so.2.0.0         | 88f5f1215381087737b262747ff2901260c0fef4 |    aiv-mp-hardknott.mp1     |        |
  |     libapu_mdw_batch.so.2.0.0      | 88f5f1215381087737b262747ff2901260c0fef4 |    aiv-mp-hardknott.mp1     |        |
  |      libapusys_edma.so.3.0.0       | 88f5f1215381087737b262747ff2901260c0fef4 |    aiv-mp-hardknott.mp1     |        |
  |        libmdla_ut.so.2.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |       alps-mp-t0.mp1        |        |
  |        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |       alps-mp-t0.mp1        |        |
  |          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |       alps-mp-t0.mp5        |        |
  | libneuronusdk_runtime.mtk.so.6.3.3 | dff37081fa477fd861f859e782c4b12bcd71ab66 |           NP6_0             |        | 
  | libneuronusdk_adapter.mtk.so.6.3.3 | dff37081fa477fd861f859e782c4b12bcd71ab66 |           NP6_0             |        | 
  |     libneuron_platform.vpu.so      | dff37081fa477fd861f859e782c4b12bcd71ab66 |           NP6_0             |        | 
  |             ncc-tflite             | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |
  |              neuronrt              | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |
  |         runtime_api_sample         | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |


Changes:
- Turn off libcmdl warning log if GPU is disabled
- Update midware library

## 2023/12/1

Commit: 687d27eedb3007c18dc9dfad4a985c98079501e8

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev

- Branch: NP6_0

- Commit:   87f52c77428c5c0bb85c38aba09ce581f515a41e

  |               Binary               |                  Commit                  |         Repo/Branch         | Module |
  | :--------------------------------: | :--------------------------------------: | :-------------------------: | :----: |
  |        libapu_mdw.so.2.0.0         | 57b1d899ada6731c1cc839305ef9dad7acfc3bde |    aiv-mp-hardknott.mp1     |        |
  |     libapu_mdw_batch.so.2.0.0      | 57b1d899ada6731c1cc839305ef9dad7acfc3bde |    aiv-mp-hardknott.mp1     |        |
  |      libapusys_edma.so.3.0.0       | 57b1d899ada6731c1cc839305ef9dad7acfc3bde |    aiv-mp-hardknott.mp1     |        |
  |        libmdla_ut.so.2.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |       alps-mp-t0.mp1        |        |
  |        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |       alps-mp-t0.mp1        |        |
  |          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |       alps-mp-t0.mp5        |        |
  | libneuronusdk_runtime.mtk.so.6.3.3 | 3522125c1d0722c9eb5cce1f5ed48e435761b8b8 | neuropilot_aiv/neuron/NP6_0 |        | 
  | libneuronusdk_adapter.mtk.so.6.3.3 | 3522125c1d0722c9eb5cce1f5ed48e435761b8b8 | neuropilot_aiv/neuron/NP6_0 |        | 
  |     libneuron_platform.vpu.so      | 3522125c1d0722c9eb5cce1f5ed48e435761b8b8 | neuropilot_aiv/neuron/NP6_0 |        | 
  |             ncc-tflite             | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |
  |              neuronrt              | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |
  |         runtime_api_sample         | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |           NP6_0             |        |


Changes:
- Fix VPU XRP 3.0 ImportBuffer security issue
- neuron: Fix coverity
- neuron NP 6.0.3 official release


## 2023/08/10

Commit:   3b73fca6a790cebedd298253ce97496257361f7b

Changes:

- Fix syntax error in label_image.py

## 2023/07/31

Commit:   f6409400b13be0971d682592600b5048a2847575

Changes:

- Update demo script to remove hardcoded python script path
- Update benchmark script to remove hardcoded python script path
- Update benchmark script to add error handling for missing input file

## 2023/07/26

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev

- Branch: NP6_0

- Commit:   0a2ec1cdd662d3e980cd7488ba9aa094283e1d9b

  |               Binary               |                  Commit                  |        Branch        | Module |
  | :--------------------------------: | :--------------------------------------: | :------------------: | :----: |
  |        libapu_mdw.so.2.0.0         | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
  |     libapu_mdw_batch.so.2.0.0      | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
  |      libapusys_edma.so.3.0.0       | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
  |        libmdla_ut.so.2.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
  |        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
  |          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |    alps-mp-t0.mp5    |        |
  | libneuronusdk_runtime.mtk.so.6.2.3 | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
  | libneuronusdk_adapter.mtk.so.6.2.3 | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
  |     libneuron_platform.vpu.so      | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
  |             ncc-tflite             | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
  |              neuronrt              | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
  |         runtime_api_sample         | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |

  Changes:

  -  Remove misleading warning messages and print detailed information for easier debugging
     - Don't try to load libcmdl library if GPU isn't enabled
     - Print detailed error message when CONV2D check filter fail

## 2023/07/21

Commit:  a3a3a147b562b96785ebdaccadbfb2da619ff451

- Binaries source:

  - Repo: aiot_int/bsp/neuron-pilot-dev
  - Branch: NP6_0
  - Commit:  31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b

|               Binary               |                  Commit                  |        Branch        | Module |
| :--------------------------------: | :--------------------------------------: | :------------------: | :----: |
|        libapu_mdw.so.2.0.0         | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|     libapu_mdw_batch.so.2.0.0      | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|      libapusys_edma.so.3.0.0       | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|        libmdla_ut.so.2.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
|        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
|          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |    alps-mp-t0.mp5    |        |
| libneuronusdk_runtime.mtk.so.6.2.3 | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
| libneuronusdk_adapter.mtk.so.6.2.3 | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
|     libneuron_platform.vpu.so      | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
|             ncc-tflite             | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
|              neuronrt              | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |
|         runtime_api_sample         | 31b04ec356c3d2ca8319d74b29cc5d2965bf7e1b |        NP6_0         |        |

Changes:

-  Refine error message to print detailed information for easier debugging
  - Refine error message when checking bias quantization
  * Print detailed error message when CONV_2D check failed
  * Print detailed error message when PRelu layer verify failed

## 2023/02/08

Commit:  44fe607495911c4617252d9da39b9d221788aeb1 

Changes: 

- Update demo and benchmark scripts

## 2023/02/02

Commit: e8af43794f1b19630ec32594c486be2a666b3d65

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: NP6_0
- Commit: bd10bcea7fbe2ef99d285900b22e2f1ea07d6c2d

|               Binary               |                  Commit                  |        Branch        | Module |
| :--------------------------------: | :--------------------------------------: | :------------------: | :----: |
|        libapu_mdw.so.2.0.0         | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|     libapu_mdw_batch.so.2.0.0      | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|      libapusys_edma.so.3.0.0       | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|        libmdla_ut.so.2.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
|        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
|          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |    alps-mp-t0.mp5    |        |
| libneuronusdk_runtime.mtk.so.6.2.3 | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
| libneuronusdk_adapter.mtk.so.6.2.3 | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
|     libneuron_platform.vpu.so      | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
|             ncc-tflite             | e7d2ffcfff0e17a3e81fc4c6a3020f7328e1a92c |        NP6_0         |        |
|              neuronrt              | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
|         runtime_api_sample         | e7d2ffcfff0e17a3e81fc4c6a3020f7328e1a92c |        NP6_0         |        |

Changes:

- Support universal neuron: 

  For mt8195 and mt8188, use one single user space library. In user space, there are 2 parts: neuron and apusys midware. 

  Before, mt8195 and mt8188 apusys kernel driver are ported from different android branches (aka different neuropilot version):

  - mt8195: neuropilot 5, android R.
  - mt8188: neuropilot 6, android T.

  So they used different neuron libraries and apusys midware libraries on mt8195 and mt8188.

  Now we upgrade mt8195 apusys kernel driver from android R to android T, makes mt8195 and mt8188 use the same kernel driver, so they can use the same neuron and apusys midware user space library.

  Below are changes:

  - Remove mt8195/mt8188 folder
  - Create mdw folder to place apusys midware user space libraries
  - Create neuron folder to place neuron user space libraries
  - Create sample folder to place demo and benchmark scripts

  ```
  ├── LICENSE
  ├── Makefile
  ├── mdw
  │   └── android13
  │       ├── README.md
  │       └── usr
  │           ├── lib64
  │           │   ├── libapu_mdw_batch.so -> libapu_mdw_batch.so.2.0.0
  │           │   ├── libapu_mdw_batch.so.2 -> libapu_mdw_batch.so.2.0.0
  │           │   ├── libapu_mdw_batch.so.2.0.0
  │           │   ├── libapu_mdw.so -> libapu_mdw.so.2.0.0
  │           │   ├── libapu_mdw.so.2 -> libapu_mdw.so.2.0.0
  │           │   ├── libapu_mdw.so.2.0.0
  │           │   ├── libapusys_edma.so -> libapusys_edma.so.3.0.0
  │           │   ├── libapusys_edma.so.3 -> libapusys_edma.so.3.0.0
  │           │   ├── libapusys_edma.so.3.0.0
  │           │   ├── libmdla_ut.so.2 -> libmdla_ut.so.2.0.0
  │           │   ├── libmdla_ut.so.2.0.0
  │           │   ├── libmdla_ut.so.3 -> libmdla_ut.so.3.0.0
  │           │   ├── libmdla_ut.so.3.0.0
  │           │   ├── libvpu5.so -> libvpu5.so.5.0.0
  │           │   ├── libvpu5.so.5 -> libvpu5.so.5.0.0
  │           │   └── libvpu5.so.5.0.0
  │           └── sbin
  │               ├── apu_mdw_test
  │               ├── edma_test
  │               ├── mdla2_player
  │               ├── mdla3_player
  │               └── vpu5_test
  ├── neuron
  │   └── 6
  │       └── usr
  │           ├── include
  │           │   └── neuron
  │           │       └── api
  │           │           ├── DLAMuxerFence.h
  │           │           ├── DLAMuxer.h
  │           │           ├── Fence.h
  │           │           ├── Misc.h
  │           │           ├── NeuronAdapter.h
  │           │           ├── RuntimeAPI.h
  │           │           ├── RuntimeV2.h
  │           │           └── Types.h
  │           ├── lib64
  │           │   ├── libneuron_platform.vpu.so
  │           │   ├── libneuronusdk_adapter.mtk.so -> libneuronusdk_adapter.mtk.so.6
  │           │   ├── libneuronusdk_adapter.mtk.so.6 -> libneuronusdk_adapter.mtk.so.6.2.3
  │           │   ├── libneuronusdk_adapter.mtk.so.6.2.3
  │           │   ├── libneuronusdk_runtime.mtk.so -> libneuronusdk_runtime.mtk.so.6
  │           │   ├── libneuronusdk_runtime.mtk.so.6 -> libneuronusdk_runtime.mtk.so.6.2.3
  │           │   └── libneuronusdk_runtime.mtk.so.6.2.3
  │           └── sbin
  │               ├── ncc-tflite
  │               ├── neuronrt
  │               └── runtime_api_sample
  └── sample
      └── usr
          └── share
              ├── benchmark_dla
              │   ├── benchmark.py
              │   ├── inception_v3_quant.tflite
              │   ├── mobilenet_v2_1.0_224_quant.tflite
              │   ├── ResNet50V2_224_1.0_quant.tflite
              │   └── ssd_mobilenet_v1_coco_quantized.tflite
              └── demo_dla
                  ├── grace_hopper.jpg
                  ├── imagenet_slim_labels.txt
                  ├── label_image.py
                  └── mobilenet_v1_1.0_224_quant.tflite
  
  ```



## 2023/01/05

Commit: 961c5876e11c0c0674a8415fc068cedae9ec20fe

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: mt8195
- Commit: 18dbf3228259a4fae8b1412236d7e69e4f2905c7

Changes:

- Upgrade neuron/ncc-tflite from 5.1.1 to 5.8.3

|           Binary           |                  Commit                  |           Branch           | Module |
| :------------------------: | :--------------------------------------: | :------------------------: | :----: |
|    libapu_mdw.so.2.0.0     | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libapu_mdw_batch.so.2.0.0  | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|      libedma.so.3.0.0      | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|    libmdla_ut.s0.2.0.0     | 82ffbd65a4c209684bfae76df8a1823a2fb27b2d | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_runtime.so.5.8.3 | 1dad43edb9824eb4ff04e667f2cdbceff75c646b |           NP5_0            |        |
|      libvpu5.so.5.0.0      | 53f8320d404cb66e55dedc5709b095b3bbb93fed | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_platform.vpu.so  | 1dad43edb9824eb4ff04e667f2cdbceff75c646b |           NP5_0            |        |
|         ncc-tflite         | 21ff854ca7b1ed6754739cb2c2d551468db76e62 |           NP5_0            |        |
|          neuronrt          | 1dad43edb9824eb4ff04e667f2cdbceff75c646b |           NP5_0            |        |

## 2022/12/5

commit: f929d5176893d1d94cba2ebae3a5854710c2cf75 

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: mt8188

- Commit: f571d8bb5baa44ec8c9dca725c6c970f13c050df


Changes:

- Initial binaries

|               Binary               |                  Commit                  |        Branch        | Module |
| :--------------------------------: | :--------------------------------------: | :------------------: | :----: |
|        libapu_mdw.so.2.0.0         | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|     libapu_mdw_batch.so.2.0.0      | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|      libapusys_edma.so.3.0.0       | 57b1d899ada6731c1cc839305ef9dad7acfc3bde | aiv-mp-hardknott.mp1 |        |
|        libmdla_ut.so.3.0.0         | 78a707e2cda54500af8db2b2f1b479be9fc7ca6e |    alps-mp-t0.mp1    |        |
|          libvpu5.so.5.0.0          | 4ad18f265d97cf44ed3badc491cdceb891da9cbf |    alps-mp-t0.mp5    |        |
| libneuronusdk_runtime.mtk.so.6.2.3 | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
| libneuronusdk_adapter.mtk.so.6.2.3 | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
|     libneuron_platform.vpu.so      | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |
|             ncc-tflite             | e7d2ffcfff0e17a3e81fc4c6a3020f7328e1a92c |        NP6_0         |        |
|              neuronrt              | 38e3d4fe1fd50ec198b3b9be39bf43493c5caf77 |        NP6_0         |        |

## 2022/11/24

Commit: 0e9448659b0f42bd4f3bb7bddb57c1ec4f781696

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: mt8195

- Commit: 6fb38d868a5f26cc59bd46a06c57d9ab2cfa3237


Changes:

- Update benchmark.py, enable profile option

  - Add profile option: `--profile` to print profile information after model inference.

  - Example

    - ```
      $ python3 benchmark.py --stress --profile --count 2
      2022-04-28 23:58:55,812 [INFO] ssd_mobilenet_v1_coco_quantized.tflite, mdla2.0, inference start
      INFO: ----------------------------------------------------------------------------------------------------
      INFO:                                  Runtime Status (LEVEL 1) - Summary
      INFO: ----------------------------------------------------------------------------------------------------
      INFO:   Total Execution Time: 46.73193359375 ms over 2 times inference.
      INFO:  ====Time===, ==Ratio==, =ExeTimes=, =Time/Inf.=, ===Description===
      INFO:       10.218,     21.9%,          1, -----------, Read Compiled Network
      INFO:        1.106,      2.4%,          1, -----------, Extract Compiled Network
      INFO:        3.830,      8.2%,          2, -----------, Map Device Memory
      INFO:        3.310,      7.1%,          2, -----------, Fill Constant Data
      INFO:        0.022,      0.0%,          2, -----------, Deserialize DLA Sections
      INFO:        0.002,      0.0%,          2, -----------, Process target-specific data
      INFO:        0.116,      0.2%,          2,       0.058, Set Inference Input
      INFO:        0.427,      0.9%,         24,       0.213, Set Inference Output
      INFO:       27.702,     59.3%,          2,      13.851, Inference
      INFO:       46.732,    100.0%,          2,      14.122, Total
      INFO: ----------------------------------------------------------------------------------------------
      INFO:                                 Runtime Status (LEVEL 1) - Breakdown
      INFO: ----------------------------------------------------------------------------------------------
      INFO:   Inference: avg Time/inf.: 13.850830078125 ms over 2 times inference.
      INFO:  ====Time===, ==GraphIdx==, ==ExeIdx==, ===Description===
      INFO:       24.964,          ---,         #0, Inference
      INFO:        2.738,          ---,         #1, Inference
      INFO:  ----------------------------------------------------------------------------------------------
      INFO: ...
      ```

- Remove unused option: `--dla`, `--quit`

## 2022/10/19

Commit: 90f0bde56562e7cf98925c3e5c880448855ea038

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev

- Branch: mt8195

- Commit: a1148264129aa0617cf5defe11be05ebcd4cbc40


Changes:

- Install Neuron SDK headers to /usr/include/neuron/api/

- Refine /usr/share/benchmark_dla/benchmark.py

  - Use argparse to write user friendly command line interfaces, such as:

    ```
    options:
      -h, --help
      --auto
      --stress
      --file FILE
      --target {mdla2.0,vpu,tflite_cpu}
      --dla DLA
      --count COUNT
      --quit
    ```

    Find detailed description of each option by cmd: `python3 benchmark.py -h`

## 2022/06/23

Commit: c535f824ddbca2e815e51cfcc6e52177efd76f97

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: mt8195

- Commit: 8ae49e3c36f020b65d7926244c284a76b3477331


Changes:

- For benchmark, use proper way to query input/output info. Avoid print confusing error message, such as 'inference fail'

|           Binary           |                  Commit                  |           Branch           | Module |
| :------------------------: | :--------------------------------------: | :------------------------: | :----: |
|    libapu_mdw.so.2.0.0     | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|      libedma.so.3.0.0      | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|    libmdla_ut.s0.2.0.0     | 82ffbd65a4c209684bfae76df8a1823a2fb27b2d | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_runtime.so.5.1.1 | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |
|      libvpu5.s0.5.0.0      | 53f8320d404cb66e55dedc5709b095b3bbb93fed | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_platform.vpu.so  | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |
|         ncc-tflite         | e81c87f5607285f36aca2249e21b5dfbad88f99e |           NP5_0            |        |
|          neuronrt          | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |

## 2022/06/21

Commit: 7ea117aa3983e98fba3b9c265081d8c7fe58a67f

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: mt8195

- Commit: b64a4636499bc7161ace4c5227aef6d9011138f8


Changes:

- Fix issue about benchmark does not run when user specifies model and target
- For runtime_api_sample and neuronrt: correct searching path of libneuron_runtime.so, avoid print confusing error message, such as 'Failed to open libneuron_runtime.5.so'

|           Binary           |                  Commit                  |           Branch           | Module |
| :------------------------: | :--------------------------------------: | :------------------------: | :----: |
|    libapu_mdw.so.2.0.0     | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|      libedma.so.3.0.0      | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|    libmdla_ut.s0.2.0.0     | 82ffbd65a4c209684bfae76df8a1823a2fb27b2d | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_runtime.so.5.1.1 | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |
|      libvpu5.s0.5.0.0      | 53f8320d404cb66e55dedc5709b095b3bbb93fed | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_platform.vpu.so  | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |
|         ncc-tflite         | e81c87f5607285f36aca2249e21b5dfbad88f99e |           NP5_0            |        |
|          neuronrt          | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |

## 2022/05/27

Commit: 79fd7fe0edc81c3916fdee0f334d5244a7f627db

Binaries source:

- Repo: aiot_int/bsp/neuron-pilot-dev
- Branch: mt8195

- Commit: c325a8515b598c0226bae9cfe5618f4a7511f312


Changes:

- Initial binaries

|           Binary           |                  Commit                  |           Branch           | Module |
| :------------------------: | :--------------------------------------: | :------------------------: | :----: |
|    libapu_mdw.so.2.0.0     | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|      libedma.so.3.0.0      | 7079fc5414795ea1b6ec54fef5658a4f618ba772 | alps-mp-r0.mp6.tc33sp.ppr2 |        |
|    libmdla_ut.s0.2.0.0     | 82ffbd65a4c209684bfae76df8a1823a2fb27b2d | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_runtime.so.5.0.0 | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |
|      libvpu5.s0.5.0.0      | 53f8320d404cb66e55dedc5709b095b3bbb93fed | alps-mp-r0.mp6.tc33sp.ppr2 |        |
| libneuron_platform.vpu.so  | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |
|         ncc-tflite         | e81c87f5607285f36aca2249e21b5dfbad88f99e |           NP5_0            |        |
|          neuronrt          | 94c28521c00482a9dd068d74455f263ea81bd8c2 |         NP5_0_8139         |        |